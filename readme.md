# [IDP ENGINEERING LOGBOOK](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/tree/main)


## Self Introduction

### _Greetings!_

Hello, my name is Nurul Aleesya Binti Hilman Syahputra or 
you can call me **Aleesya**, though I am more comfortable with
**Eesya**. I am from Semenyih, Selangor. My hobby includes 
~~watching Netflix~~ visual learning and listening to music! :)


Listed below are some of my strength and weaknesses.

#### STRENGTH

- empathetic
- organized
- honest
- perfectionist

#### WEAKNESS

- procrastinator
- loses motivation easily
- sensitive
- not comfortable taking risks

And for the table section, a shameless plug of my social media
accounts. If you're into music like I do, feel free to follow me on Spotify!
I would ***LOVE*** to see what you're into! :D

| PLUGINS | LINK |
| ------ | ------ |
| Spotify | [aleesya](https://open.spotify.com/user/q2hjdk0zkdoxbbl1rce6nccje?si=b68a04679d3c4f75) |
| Instagram  | **aleesya___**|




>  ✨ _Merci_ , have a great day! 
<img src="/uploads/35e8af2d4122a53259818e8686a9c08f/IMG_4702.png" alt="IMG_4702" width="400"/>






















